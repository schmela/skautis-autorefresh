// ==UserScript==
// @id             skaut.cz autorefresh
// @name           skaut.cz autorefresh
// @version        1.0
// @namespace      skautis  
// @author         schmela
// @description    zabranuje odhlaseni na vybranych *.skaut.cz strankach
// @include        https://is.skaut.cz/*
// @include        http://krizovatka.skaut.cz/*
// @include        http://teepek.cz
// @include        http://junshop.cz
// @grant          none
// ==/UserScript==


// ----------------------------------
// MODUL AUTO-REFRESH
// ----------------------------------
// kazdych 14 minut stahne hlavicku stranky, takze nedojde k odhlaseni


function keep_alive() {
  http_request = new XMLHttpRequest();
  http_request.open('HEAD', '/');
  http_request.send(null);
};

setInterval(keep_alive, 840000); // 14min * 60s * 1000ms = 840 000 ms